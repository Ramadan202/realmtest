package com.example.limo2.testrealmdb;
import android.content.Context;
import android.graphics.Color;

import cn.pedant.SweetAlert.SweetAlertDialog;



/**
 * Created by Ahmed on 12/4/2016.
 */

public class Messenger {
    public static SweetAlertDialog getWarningDialogInstance(Context context, String title, String msg,
                                                            String btnTxt, boolean cancelable,
                                                            SweetAlertDialog.OnSweetClickListener listener) {
        SweetAlertDialog dialog= new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                .setTitleText(title)
                .setContentText(msg)
                .setConfirmText(btnTxt)
                .setConfirmClickListener(listener);
        dialog.setCancelable(cancelable);
        return dialog;

    }

    public static SweetAlertDialog SweetprogressDialogShow(Context context) {

        SweetAlertDialog dialog= new SweetAlertDialog(context, SweetAlertDialog.PROGRESS_TYPE);
        dialog.getProgressHelper().setBarColor(Color.parseColor("#e01010"));
        dialog.setTitleText("Loading......");
        dialog.setCancelable(false);
        return dialog;

    }

    public static SweetAlertDialog SweetSimCardDialogShow(Context context, SweetAlertDialog.OnSweetClickListener listener) {

        SweetAlertDialog dialog= new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE).setTitleText("SIM Card")
                .setContentText("Please enter the Sim Card to can use the features of Limozein").setConfirmClickListener(listener);;

        dialog.setCancelable(false);
        return dialog;

    }





    public static SweetAlertDialog getGeneralWarningDialogInstance(Context context, String title, String msg, boolean cancelable,
                                                                   SweetAlertDialog.OnSweetClickListener listener, int type) {
        SweetAlertDialog dialog = null;
        if(type==0)
        {
            dialog =new SweetAlertDialog(context)
                    .setContentText(msg);
            dialog.setCancelable(cancelable);

        }
        else if(type==1)
        {
            dialog= new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE)
                    .setTitleText(title)
                    .setContentText(msg)
                    .setConfirmClickListener(listener);
            dialog.setCancelable(cancelable);

        }
        return dialog;

    }


}
