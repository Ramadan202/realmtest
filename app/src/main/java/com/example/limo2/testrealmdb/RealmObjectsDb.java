package com.example.limo2.testrealmdb;

import io.realm.Realm;
import io.realm.RealmAsyncTask;

/**
 * Created by limo2 on 1/3/18.
 */

public class RealmObjectsDb {
    private static Realm realm;
    static RealmAsyncTask transaction;
    public static Realm getInstanceOfRealm() {
        if(realm==null)
        {
            realm=Realm.getDefaultInstance();
        }
      return realm;
    }

    public static void addObjectToRealm(final User user)
    {
        getInstanceOfRealm().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(user);
            }
        });
    }

    public static void addObject(final User user, Realm.Transaction.OnSuccess onSuccess, Realm.Transaction.OnError onError)
    {
        transaction=  getInstanceOfRealm().executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(user);
            }
        },onSuccess,onError);
    }

    public static void addObjectCar(final CarDetials carDetials, Realm.Transaction.OnSuccess onSuccess, Realm.Transaction.OnError onError)
    {
        transaction=  getInstanceOfRealm().executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.copyToRealmOrUpdate(carDetials);
            }
        },onSuccess,onError);
    }

    public static User getUserObject()
    {
        User user=getInstanceOfRealm().where(User.class).findFirst();
        return user;
    }

    public static void updateSpecificUser(final int sqlId, Realm.Transaction.OnSuccess onSuccess, Realm.Transaction.OnError onError)
    {
        transaction=getInstanceOfRealm().executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                User user = realm.where(User.class).findFirst();
                if(user!=null) {
                    user.setLastName("Eid");
                }
            }
        },onSuccess,onError);
    }

    public static void deleteSpecificObject(Realm.Transaction.OnSuccess onSuccess, Realm.Transaction.OnError onError)
    {
        transaction=getInstanceOfRealm().executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                User user = realm.where(User.class).findFirst();
                if(user!=null) {
                    user.deleteFromRealm();
                }
            }
        },onSuccess,onError);

    }



    public static void onDestriy()
    {
        if (transaction != null && !transaction.isCancelled()) {
            transaction.cancel();
        }
    }


}
