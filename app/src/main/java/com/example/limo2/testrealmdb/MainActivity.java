package com.example.limo2.testrealmdb;

import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.realm.Realm;
import io.realm.RealmAsyncTask;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;
import io.realm.Sort;

public class MainActivity extends AppCompatActivity {
    private Realm realm;
    private RealmResults<User> userRealmResults;
    private RealmChangeListener realmChangeCategoryListener;
    RealmAsyncTask transaction;
    RealmAsyncTask showTransaction;
//    RealmAsyncTask UpdateTransaction;
//    RealmAsyncTask deleteTransaction;
    private SweetAlertDialog UserNotSimExistMacWarning;
    String name=null;
    User user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
//                .setTitleText("Are you sure?")
//                .setContentText("Won't be able to recover this file!")
//                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
//                    @Override
//                    public void onClick(SweetAlertDialog sDialog) {
//                        sDialog.dismissWithAnimation();
//                    }
//                })
//                .show();

//       final SweetAlertDialog alertDialog = new SweetAlertDialog(MainActivity.this,SweetAlertDialog.WARNING_TYPE);
//        alertDialog .setTitleText("Title");
//        alertDialog .setContentText("Disabled button dialog");
//        alertDialog .setConfirmText("Confirm");
//        alertDialog .setCancelText("Cancel");
//        alertDialog.show();
//
//
//        Button btn = (Button) alertDialog.findViewById(R.id.confirm_button);
//        btn.setBackgroundColor(ContextCompat.getColor(MainActivity.this,R.color.colorPrimary));
//
//        Button btnn = (Button) alertDialog.findViewById(R.id.cancel_button);
//        btnn.setBackgroundColor(ContextCompat.getColor(MainActivity.this,R.color.colorPrimary));




//        UserNotSimExistMacWarning = Messenger.SweetSimCardDialogShow(this, new SweetAlertDialog.OnSweetClickListener() {
//            @Override
//            public void onClick(SweetAlertDialog sweetAlertDialog) {
//                finish();
//                moveTaskToBack(false);
//
//            }
//        });
//        UserNotSimExistMacWarning.show();

//        btn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Toast.makeText(MainActivity.this, "yyyy", Toast.LENGTH_SHORT).show();
//            }
//        });
//
//
//        btnn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(MainActivity.this, "nnnn", Toast.LENGTH_SHORT).show();
//                alertDialog.dismissWithAnimation();
//
//            }
//        });

        realm = Realm.getDefaultInstance();

        Button insert=findViewById(R.id.insertData);
        Button update=findViewById(R.id.UpdateData);
        Button show=findViewById(R.id.ShowtData);
        Button delete=findViewById(R.id.DELETEData);

        insert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                for (int i=0;i<1;i++)
//                {
//                    final User user=new User();
//                    user.setSqlId(i+1);
//                    user.setFirstName("Ramadan");
//                    realm.executeTransaction(new Realm.Transaction() {
//                        @Override
//                        public void execute(Realm realm) {
//
//                            realm.copyToRealmOrUpdate(user);
//                        }
//                    });

                CarDetials carDetials=new CarDetials();
                carDetials.setTripClassID(1);
                carDetials.setTripClass("economy");
                RealmObjectsDb.addObjectCar(carDetials, new Realm.Transaction.OnSuccess() {
                    @Override
                    public void onSuccess() {
                        Toast.makeText(MainActivity.this, "Car added Done", Toast.LENGTH_SHORT).show();

                    }
                }, new Realm.Transaction.OnError() {
                    @Override
                    public void onError(Throwable error) {

                    }
                });
                User userone=new User();
                userone.setSqlId(1);

                userone.setLastName("Sayed");


                 RealmObjectsDb.addObject(userone, new Realm.Transaction.OnSuccess() {
                     @Override
                     public void onSuccess() {
                         Toast.makeText(MainActivity.this, "Done", Toast.LENGTH_SHORT).show();
                     }
                 }, new Realm.Transaction.OnError() {
                     @Override
                     public void onError(Throwable error) {

                     }
                 });
//                 transaction=   realm.executeTransactionAsync(new Realm.Transaction() {
//                     @Override
//                     public void execute(Realm realm) {
//                         realm.copyToRealmOrUpdate(user);
//                     }
//                 }, new Realm.Transaction.OnSuccess() {
//                     @Override
//                     public void onSuccess() {
//                         Toast.makeText(MainActivity.this, "Done", Toast.LENGTH_SHORT).show();
//                     }
//                 }, new Realm.Transaction.OnError() {
//                     @Override
//                     public void onError(Throwable error) {
//
//                     }
//                 });




//                    transaction=   realm.executeTransactionAsync(new Realm.Transaction() {
//                        @Override
//                        public void execute(Realm realm) {
//                            realm.copyToRealmOrUpdate(user);
//                        }
//                    },null,null);
//
               }


            //}
        });

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                User user=new User();
//                user.setSqlId(1);
//
//                user.setFirstName("Ramadan");
//                user.setLastName("Eid");
//
//
//                RealmObjectsDb.addObject(user, new Realm.Transaction.OnSuccess() {
//                    @Override
//                    public void onSuccess() {
//                        Toast.makeText(MainActivity.this, "Done", Toast.LENGTH_SHORT).show();
//                    }
//                }, new Realm.Transaction.OnError() {
//                    @Override
//                    public void onError(Throwable error) {
//
//                    }
//                });

                if(RealmObjectsDb.getUserObject()!=null) {
                    RealmObjectsDb.updateSpecificUser(RealmObjectsDb.getUserObject().getSqlId(), new Realm.Transaction.OnSuccess() {
                        @Override
                        public void onSuccess() {
                            Toast.makeText(MainActivity.this, "UpdateDone", Toast.LENGTH_SHORT).show();
                        }
                    }, new Realm.Transaction.OnError() {
                        @Override
                        public void onError(Throwable error) {

                        }
                    });
                }
//                transaction=realm.executeTransactionAsync(new Realm.Transaction() {
//                    @Override
//                    public void execute(Realm realm) {
//                        User user = realm.where(User.class).equalTo("sqlId", 1).findFirst();
//                        if(user!=null) {
//                            user.setLastName("Eid");
//                        }
//                    }
//                }, new Realm.Transaction.OnSuccess() {
//                    @Override
//                    public void onSuccess() {
//                        Toast.makeText(MainActivity.this, "UpdateDone", Toast.LENGTH_SHORT).show();
//
//                    }
//                }, new Realm.Transaction.OnError() {
//                    @Override
//                    public void onError(Throwable error) {
//
//                    }
//                });
//
//                realm.executeTransaction(new Realm.Transaction() {
//                    @Override
//                    public void execute(Realm realm) {
//                        User user = realm.where(User.class).equalTo("sqlId", 1).findFirst();
//                        if(user!=null) {
//                            user.setLastName("Eid");
//                        }
//                    }
//                });


            }
        });

        show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                   Realm.getDefaultInstance().refresh();
//                showTransaction=realm.executeTransactionAsync(new Realm.Transaction() {
//                    @Override
//                    public void execute(Realm realm) {
//
//                        userRealmResults = realm.where(User.class).findAllAsync();
//
//                        if(userRealmResults!=null) {
//                            for (User user : userRealmResults) {
//                                if(user.getLastName()==null)
//                                {
//                                    Toast.makeText(MainActivity.this, "Null", Toast.LENGTH_SHORT).show();
//                                }
//                                Toast.makeText(MainActivity.this, user.getLastName(), Toast.LENGTH_SHORT).show();
//                                Toast.makeText(MainActivity.this, "" + user.getSqlId(), Toast.LENGTH_SHORT).show();
//
//                            }
//                        }
//                    }
//                },null,null);


              //  User user=realm.where(User.class).findFirst();
                User user=RealmObjectsDb.getUserObject();
                if(user!=null) {
                    Toast.makeText(MainActivity.this, "" + user.getSqlId(), Toast.LENGTH_SHORT).show();
                    Toast.makeText(MainActivity.this, user.getFirstName(), Toast.LENGTH_SHORT).show();
                    Toast.makeText(MainActivity.this, user.getLastName(), Toast.LENGTH_SHORT).show();
                }
//                userRealmResults = realm.where(User.class).findAllAsync();
//
//                if(userRealmResults!=null) {
//                    for (User user : userRealmResults) {
//                        if(user.getLastName()==null)
//                        {
//                            Toast.makeText(MainActivity.this, "Null", Toast.LENGTH_SHORT).show();
//                        }
//                        Toast.makeText(MainActivity.this, user.getLastName(), Toast.LENGTH_SHORT).show();
//                        Toast.makeText(MainActivity.this, "" + user.getSqlId(), Toast.LENGTH_SHORT).show();
//
//                    }
//                }
//
//                setupListener();
//                userRealmResults.addChangeListener(realmChangeCategoryListener);
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                RealmObjectsDb.deleteSpecificObject(new Realm.Transaction.OnSuccess() {
                    @Override
                    public void onSuccess() {
                        Toast.makeText(MainActivity.this, "DeleteDone", Toast.LENGTH_SHORT).show();
                    }
                }, new Realm.Transaction.OnError() {
                    @Override
                    public void onError(Throwable error) {

                    }
                });
//                final User user = realm.where(User.class).equalTo("sqlId", 1).findFirst();
//
//                transaction=realm.executeTransactionAsync(new Realm.Transaction() {
//                    @Override
//                    public void execute(Realm realm) {
//                        User user = realm.where(User.class).equalTo("sqlId", 1).findFirst();
//                        if(user!=null)
//                        {
//                            user.deleteFromRealm();
//                        }
//                    }
//                }, new Realm.Transaction.OnSuccess() {
//                    @Override
//                    public void onSuccess() {
//
//                        Toast.makeText(MainActivity.this, "DeleteDone", Toast.LENGTH_SHORT).show();
//                    }
//                }, new Realm.Transaction.OnError() {
//                    @Override
//                    public void onError(Throwable error) {
//
//                    }
//                });

//                realm.executeTransaction(new Realm.Transaction() {
//                    @Override
//                    public void execute(Realm realm) {
//                        if(user!=null)
//                        {
//                            user.deleteFromRealm();
//                        }
//
//                    }
//                });
            }
        });

    }


/*
To find all users named John or Peter, you would write:

Copy to clipboard// Build the query looking at all users:
RealmQuery<User> query = realm.where(User.class);

// Add query conditions:
query.equalTo("name", "John");
query.or().equalTo("name", "Peter");

// Execute the query:
RealmResults<User> result1 = query.findAll();

// Or alternatively do the same all at once (the "Fluent interface"):
RealmResults<User> result2 = realm.where(User.class)
                                  .equalTo("name", "John")
                                  .or()
                                  .equalTo("name", "Peter")
                                  .findAll();
This gives you a new instance of the class RealmResults, containing the users with the name John or Peter.

The method findAll executes the query; RealmQuery includes a family of findAll methods:

findAll finds all objects that meet the query conditions
findAllAsync operates asynchronously on a background thread
findFirst (and findFirstAsync) find the first object that meets the query conditions
For full details, dive into the RealmQuery API reference.

Queries return a list of references to the matching objects, so you work directly with the original objects that matches your query. RealmResults inherits from AbstractList and behaves in similar ways. For example, RealmResults are ordered, and you can access the individual objects through an index. If a query has no matches, the returned RealmResults object will be a list of size(0) (not null).

If you wish to modify or delete objects in a RealmResults set, you must do so in a write transaction.
 */




//    private void setupListener() {
//        realmChangeCategoryListener = new RealmChangeListener() {
//            @Override public void onChange() {
//                Timber.d("onChange(): category change");
//                listCategory.clear();
//                for (Category data : categoryRealmResults) {
//                    Timber.d("onChange(): " + data.getName() + " - " + data.getCreatedAt().toString());
//                    listCategory.add(data.getName());
//                }
//                spCategoryAdapter.notifyDataSetChanged();
//            }
//        };
//
//        realmChangeBookListener = new RealmChangeListener() {
//            @Override public void onChange() {
//                bookRealmResults.sort("createdAt", Sort.DESCENDING);
//                adapter.notifyDataSetChanged();
//            }
//        };
//    }


    private void setupListener() {
        realmChangeCategoryListener=new RealmChangeListener() {
            @Override
            public void onChange(Object o) {
                if(userRealmResults!=null)
                userRealmResults.sort("sqlId", Sort.ASCENDING);
                Toast.makeText(MainActivity.this, "Listner", Toast.LENGTH_SHORT).show();
            }
        };
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        RealmObjectsDb.onDestriy();

//        if (transaction != null && !transaction.isCancelled()) {
//            transaction.cancel();
//        }
//        if(userRealmResults!=null)
//        {
//            userRealmResults.removeChangeListener(realmChangeCategoryListener);
//        }
//        if(realm!=null)
//        {
//            realm.close();
//        }
//


    }
}
