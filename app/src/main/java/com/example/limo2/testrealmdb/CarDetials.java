package com.example.limo2.testrealmdb;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by limo2 on 12/26/17.
 */

public class CarDetials extends RealmObject {
    @SerializedName("tripClassID")
    @PrimaryKey
    private int tripClassID;
    @SerializedName("tripClass")
    private String tripClass;

    @SerializedName("airportEntranceValue")
    private float airportEntranceValue;


    public int getTripClassID() {
        return tripClassID;
    }

    public void setTripClassID(int tripClassID) {
        this.tripClassID = tripClassID;
    }

    public String getTripClass() {
        return tripClass;
    }

    public void setTripClass(String tripClass) {
        this.tripClass = tripClass;
    }

    public float getAirportEntranceValue() {
        return airportEntranceValue;
    }

    public void setAirportEntranceValue(float airportEntranceValue) {
        this.airportEntranceValue = airportEntranceValue;
    }


}
