package com.example.limo2.testrealmdb;

import android.content.Context;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmMigration;

/**
 * Created by aymanabouzeid on 16/01/17.
 */

public class RealmHelper {
    public static void initRealm(Context context) {
        Realm.init(context);
    }

    public static void setDefaultConfiguration(RealmConfiguration realmConfiguration) {
        Realm.setDefaultConfiguration(realmConfiguration);
    }

    public static RealmConfiguration getRealmConfiguration(String name, int version,
                                                           RealmMigration realmMigration, Object baseModel, Object... models) {
        RealmConfiguration.Builder builder = new RealmConfiguration.Builder().name(name).schemaVersion(version);
        if (realmMigration != null) {
            builder.migration(realmMigration);
        }

        if (baseModel != null) {
            builder.modules(baseModel, models);
        }
        return builder.build();
    }
}
