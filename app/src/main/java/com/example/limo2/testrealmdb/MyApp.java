package com.example.limo2.testrealmdb;

import android.app.Application;

/**
 * Created by limo2 on 12/26/17.
 */

public class MyApp extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        initRealm();
    }

    private void initRealm() {
        RealmHelper.initRealm(this);

            RealmHelper.setDefaultConfiguration(RealmHelper.getRealmConfiguration(RealmConfig.NAME, RealmConfig.VERSION, null, new RealmModules()));

    }
}
